package workshop.katakaio.web;

import workshop.katakaio.core.StoryCreation;
import workshop.katakaio.core.StoryRequest;

public class StoryController {
    private StoryCreation storyService;

    public StoryController(StoryCreation storyService) {
        this.storyService = storyService;
    }

    public StoryRequest create(StoryRequest request) {
        storyService.create(request);
        return request;
    }
}
