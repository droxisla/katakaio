package workshop.katakaio.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ChartController {

    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("chart");
        modelAndView.addObject("days", List.of("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));

        modelAndView.addObject("guidelineValues", List.of(144.00, 144.00, 123.00, 104.00, 91.00, 70.00, 49.00, 28.00, 9.00, 0.00, 0.00));
        modelAndView.addObject("todoValues", List.of(128.00, 128.00, 86.67, 44.50, 42.50, 33.50));
        modelAndView.addObject("inProgressValues", List.of(0.00, 0.00, 5.00, 35.58, 14.50, 18.50));
        modelAndView.addObject("doneValues", List.of(0.00, 0.00, 16.46, 38.58, 54.58, 71.41));

        modelAndView.addObject("devGuidelineValues", List.of(104.00, 104.00, 89.00, 76.00, 67.00, 52.00, 37.00, 22.00, 9.00, 0.00, 0.00));
        modelAndView.addObject("devTodoValues", List.of(96.00, 96.00, 65.67, 31.50, 29.50, 23.50));
        modelAndView.addObject("devInProgressValues", List.of(0.00, 0.00, 1.92, 30.83, 14.50, 18.50));
        modelAndView.addObject("devDoneValues", List.of(0.00, 0.00, 9.45, 25.74, 37.24, 47.24));

        modelAndView.addObject("testGuidelineValues", List.of(40.00, 40.00, 34.00, 28.00, 24.00, 18.00, 12.00, 6.00, 0.00, 0.00, 0.00));
        modelAndView.addObject("testTodoValues", List.of(32.00, 32.00, 21.00, 13.00, 13.00, 10.00));
        modelAndView.addObject("testInProgressValues", List.of(0.00, 0.00, 3.08, 4.75, 0.00, 0.00));
        modelAndView.addObject("testDoneValues", List.of(0.00, 0.00, 7.01, 12.84, 17.34, 24.17));

        return modelAndView;
    }
}
