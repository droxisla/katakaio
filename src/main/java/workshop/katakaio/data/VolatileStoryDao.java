package workshop.katakaio.data;

import workshop.katakaio.core.Story;
import workshop.katakaio.core.StoryDao;

import java.util.LinkedHashMap;
import java.util.Map;

public class VolatileStoryDao implements StoryDao {
    private Map<String, Story> stories;

    public VolatileStoryDao() {
        stories = new LinkedHashMap<>();
    }

    @Override
    public void create(Story request) {
        stories.put(request.getId(), request);
    }

    @Override
    public Story findById(String id) {
        return stories.get(id);
    }
}
