package workshop.katakaio.core;

public interface StoryCreation {
    void create(StoryRequest request);
}
