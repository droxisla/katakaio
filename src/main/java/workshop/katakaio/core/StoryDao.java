package workshop.katakaio.core;

public interface StoryDao {
    void create(Story request);

    Story findById(String id);
}
