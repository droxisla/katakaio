package workshop.katakaio.core;

public class StoryRequest {
    public String id;
    public String title;
    public String type;
    public Integer points;
    public Integer estimate;
    public Integer remaining;
    public Integer spent;
    public String status;
}
