package workshop.katakaio.core;

public class Story {
    private String id;
    private String title;
    private String type;
    private Integer points;
    private Integer estimate;
    private Integer remaining;
    private Integer spent;
    private String status;

    Story(StoryRequest request) {
        this.id = request.id;
        this.title = request.title;
        this.type = request.type;
        this.points = request.points;
        this.estimate = request.estimate;
        this.remaining = request.remaining;
        this.spent = request.spent;
        this.status = request.status;
    }

    public Story() {

    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public Integer getSpent() {
        return spent;
    }

    public String getStatus() {
        return status;
    }
}
