package workshop.katakaio.core;

public class DefaultStoryCreation implements StoryCreation {
    private StoryDao dao;

    public DefaultStoryCreation(StoryDao dao) {
        this.dao = dao;
    }

    @Override
    public void create(StoryRequest request) {
        Story story = new Story(request);
        dao.create(story);
    }
}
