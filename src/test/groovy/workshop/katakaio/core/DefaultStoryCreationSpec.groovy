package workshop.katakaio.core

import spock.lang.Specification
import workshop.katakaio.data.VolatileStoryDao

class DefaultStoryCreationSpec extends Specification {
    StoryDao dao = new VolatileStoryDao()
    StoryCreation service = new DefaultStoryCreation(dao)

    def 'Should create story'() {
        given:
        StoryRequest request = new StoryRequest(
                id: 'id',
                title: 'title',
                type: 'type',
                points: 0,
                estimate: 3,
                remaining: 2,
                spent: 1,
                status: 'status'
        )

        when:
        service.create(request)

        then:
        Story created = dao.findById(request.id)
        created.id == request.id
        created.title == request.title
        created.type == request.type
        created.points == request.points
        created.estimate == request.estimate
        created.remaining == request.remaining
        created.spent == request.spent
        created.status == request.status
    }
}