package workshop.katakaio.web

import spock.lang.Specification
import workshop.katakaio.core.StoryCreation
import workshop.katakaio.core.StoryRequest

class StoryControllerSpec extends Specification implements StoryCreation {
    private boolean called
    private StoryRequest request

    void setup() {
        called = false
        request = new StoryRequest(
                id: 'id',
                title: 'title',
                type: 'type',
                points: 0,
                estimate: 3,
                remaining: 2,
                spent: 1,
                status: 'status'
        )
    }

    def 'Should create story'() {
        given:
        StoryController controller = new StoryController(this)

        expect:
        controller.create(this.request) == this.request
        called
    }

    @Override
    void create(StoryRequest request) {
        assert request == this.request
        called = true
    }
}