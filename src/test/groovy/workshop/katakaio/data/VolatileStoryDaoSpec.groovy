package workshop.katakaio.data

import spock.lang.Specification
import workshop.katakaio.core.Story
import workshop.katakaio.core.StoryDao

class VolatileStoryDaoSpec extends Specification {
    StoryDao dao = new VolatileStoryDao()

    def 'Should create story'() {
        given:
        Story request = new Story(
                id: 'id',
                title: 'title',
                type: 'type',
                points: 0,
                estimate: 3,
                remaining: 2,
                spent: 1,
                status: 'status'
        )

        when:
        dao.create(request)

        then:
        dao.findById(request.id) == request
    }
}